# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the master-key package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: master-key 1.6.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-17 11:11-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/ui/dialog.blp:16
msgid "Close"
msgstr ""

#: data/ui/dialog.blp:26
msgid "Save"
msgstr ""

#: data/ui/dialog.blp:54
msgid "Domain Name"
msgstr ""

#: data/ui/dialog.blp:60 data/ui/row.blp:29
msgid "Username"
msgstr ""

#: data/ui/dialog.blp:66
msgid "Length"
msgstr ""

#: data/ui/dialog.blp:79
msgid "Version"
msgstr ""

#: data/ui/dialog.blp:93
msgid "Character set"
msgstr ""

#: data/ui/dialog.blp:96
msgid "Uppercase"
msgstr ""

#: data/ui/dialog.blp:103
msgid "Lowercase"
msgstr ""

#: data/ui/dialog.blp:110
msgid "Digits"
msgstr ""

#: data/ui/dialog.blp:117
msgid "Symbols"
msgstr ""

#: data/ui/dialog.blp:125 data/ui/setup-view.blp:23 data/ui/unlock-view.blp:24
msgid "Password"
msgstr ""

#: data/ui/dialog.blp:128
msgid "Generated password"
msgstr ""

#: data/ui/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: data/ui/help-overlay.blp:14
msgid "Toggle Primary Menu"
msgstr ""

#: data/ui/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Lock"
msgstr ""

#: data/ui/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Preferences"
msgstr ""

#: data/ui/help-overlay.blp:29
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr ""

#: data/ui/help-overlay.blp:34
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: data/ui/help-overlay.blp:40
msgctxt "shortcut window"
msgid "Passwords"
msgstr ""

#: data/ui/help-overlay.blp:43
msgctxt "shortcut window"
msgid "New Password"
msgstr ""

#: data/ui/help-overlay.blp:48
msgctxt "shortcut window"
msgid "Search"
msgstr ""

#: data/ui/main-view.blp:9
msgid "New password"
msgstr ""

#: data/ui/main-view.blp:16
msgid "Main menu"
msgstr ""

#: data/ui/main-view.blp:24
msgid "Search"
msgstr ""

#: data/ui/main-view.blp:38 data/ui/main-view.blp:43
msgid "Search passwords"
msgstr ""

#: data/ui/main-view.blp:59
msgid "No passwords yet"
msgstr ""

#: data/ui/main-view.blp:60
msgid "Add a new password or restore passwords from a file"
msgstr ""

#: data/ui/main-view.blp:63 master_key/widgets/dialog.py:56
msgid "Add password"
msgstr ""

#: data/ui/main-view.blp:80
msgid "No results found"
msgstr ""

#: data/ui/main-view.blp:81
msgid "Try a different search"
msgstr ""

#: data/ui/main-view.blp:112
msgid "Lock the application"
msgstr ""

#: data/ui/main-view.blp:118
msgid "Sorting"
msgstr ""

#: data/ui/main-view.blp:121
msgid "A-Z"
msgstr ""

#: data/ui/main-view.blp:127
msgid "Z-A"
msgstr ""

#: data/ui/main-view.blp:133
msgid "Oldest First"
msgstr ""

#: data/ui/main-view.blp:139
msgid "Newest First"
msgstr ""

#: data/ui/main-view.blp:148
msgid "Reset password"
msgstr ""

#: data/ui/main-view.blp:155
msgid "Preferences"
msgstr ""

#: data/ui/main-view.blp:160
msgid "Keyboard Shortcuts"
msgstr ""

#: data/ui/main-view.blp:165
msgid "About Master Key"
msgstr ""

#: data/ui/preferences.blp:9 data/ui/preferences.blp:13
msgid "Security"
msgstr ""

#: data/ui/preferences.blp:16
#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:12
msgid "Clipboard timeout"
msgstr ""

#: data/ui/preferences.blp:17
#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:13
msgid "Clear clipboard after X seconds"
msgstr ""

#: data/ui/preferences.blp:31
#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:17
msgid "Auto lock timeout"
msgstr ""

#: data/ui/preferences.blp:32
#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:18
msgid "Lock application after X minutes"
msgstr ""

#: data/ui/preferences.blp:46
#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:37
msgid "Lock on session lock"
msgstr ""

#: data/ui/preferences.blp:47
msgid "Automatically lock when the session is locked"
msgstr ""

#: data/ui/preferences.blp:53
msgid "Export/Import"
msgstr ""

#: data/ui/preferences.blp:57
msgid "Export"
msgstr ""

#: data/ui/preferences.blp:60 data/ui/preferences.blp:79
msgid "Password definitions"
msgstr ""

#: data/ui/preferences.blp:61
msgid "In a plain-text JSON file"
msgstr ""

#: data/ui/preferences.blp:76
msgid "Import"
msgstr ""

#: data/ui/preferences.blp:80
msgid "From a plain-text JSON file"
msgstr ""

#: data/ui/reset-dialog.blp:5
msgid "Reset Master Password?"
msgstr ""

#: data/ui/reset-dialog.blp:7
msgid "Cancel"
msgstr ""

#: data/ui/reset-dialog.blp:8
msgid "Continue"
msgstr ""

#: data/ui/reset-dialog.blp:14
msgid "I understand that all passwords will be deleted."
msgstr ""

#: data/ui/row.blp:36
msgid "Copy username"
msgstr ""

#: data/ui/row.blp:80
msgid "Delete password"
msgstr ""

#: data/ui/row.blp:91 master_key/widgets/dialog.py:58
msgid "Edit password"
msgstr ""

#: data/ui/setup-view.blp:12
msgid "Master Password Setup"
msgstr ""

#: data/ui/setup-view.blp:30
msgid "Confirm Password"
msgstr ""

#: data/ui/setup-view.blp:68
msgid "Create Database"
msgstr ""

#: data/ui/unlock-view.blp:13
msgid "Unlock Database"
msgstr ""

#: data/ui/unlock-view.blp:46
msgid "Unlock"
msgstr ""

#: data/ui/window.blp:9 data/com.gitlab.guillermop.MasterKey.desktop.in.in:3
#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:6
#: master_key/application.py:30 master_key/application.py:92
msgid "Master Key"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.desktop.in.in:4
#: master_key/application.py:93
msgid "A password manager"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:22
msgid "Window width"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:23
msgid "Default window width."
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:27
msgid "Window height"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:28
msgid "Default window height."
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:32
msgid "Window maximized"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:33
msgid "Default window maximized state."
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:38
msgid "Lock the database when the session is locked."
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:42
msgid "Sorting order"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.gschema.xml.in:43
msgid "Sorting order of the rows."
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:7
msgid "Manage passwords without saving them"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:9
msgid ""
"Master Key is a password manager application that generates and manages "
"passwords without the need to store them"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:13
msgid "Features:"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:17
msgid ""
"Passwords are generated using a combination of a master key, a site and a "
"login"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:18
msgid "Passwords are never stored"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:19
msgid "Passwords can be recreated everywhere"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:20
msgid "The accounts information is encrypted"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:21
msgid "Follows the GNOME Human Interface Guidelines"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:35
msgid "Password management page"
msgstr ""

#: data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in:39
msgid "Unlock page"
msgstr ""

#: master_key/application.py:101
msgid "translator-credits"
msgstr ""

#. translatable strings from zxcvbn
#: master_key/dummy.py:10
msgid "Use a few words, avoid common phrases."
msgstr ""

#: master_key/dummy.py:11
msgid "No need for symbols, digits, or uppercase letters."
msgstr ""

#: master_key/dummy.py:12
msgid "Add another word or two. Uncommon words are better."
msgstr ""

#: master_key/dummy.py:13
msgid "Use a longer keyboard pattern with more turns."
msgstr ""

#: master_key/dummy.py:14
msgid "Avoid repeated words and characters."
msgstr ""

#: master_key/dummy.py:15
msgid "Avoid sequences."
msgstr ""

#: master_key/dummy.py:16
msgid "Avoid recent years."
msgstr ""

#: master_key/dummy.py:17
msgid "Avoid years that are associated with you."
msgstr ""

#: master_key/dummy.py:18
msgid "Avoid dates and years that are associated with you."
msgstr ""

#: master_key/dummy.py:19
msgid "Capitalization doesn't help very much."
msgstr ""

#: master_key/dummy.py:20
msgid "All-uppercase is almost as easy to guess as all-lowercase."
msgstr ""

#: master_key/dummy.py:21
msgid "Reversed words aren't much harder to guess."
msgstr ""

#: master_key/widgets/dialog.py:185 master_key/widgets/row.py:80
msgid "Password copied"
msgstr ""

#: master_key/widgets/row.py:49
#, python-brace-format
msgid "version: {0}"
msgstr ""

#: master_key/widgets/row.py:85
msgid "Username copied"
msgstr ""

#: master_key/widgets/setup_view.py:83
msgid "Password doesn't match"
msgstr ""

#: master_key/widgets/unlock_view.py:50
msgid "Failed to unlock database. Try again."
msgstr ""

#: master_key/widgets/window.py:132
#, python-brace-format
msgid "{0} passwords deleted."
msgstr ""

#: master_key/widgets/window.py:134
#, python-brace-format
msgid "Password \"{0}\" deleted."
msgstr ""

#: master_key/widgets/window.py:142
msgid "Undo"
msgstr ""
