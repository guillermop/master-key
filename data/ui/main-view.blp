using Gtk 4.0;
using Adw 1;

template $MainView: Adw.Bin {
  Adw.ToolbarView {
    [top]
    Adw.HeaderBar _header_bar {
      Button _add_button {
        tooltip-text: _("New password");
        action-name: 'win.add';
        icon-name: 'list-add-symbolic';
      }

      [end]
      MenuButton _menu_button {
        tooltip-text: _("Main menu");
        menu-model: app-menu;
        icon-name: 'open-menu-symbolic';
        primary: true;
      }

      [end]
      ToggleButton _search_button {
        tooltip-text: _("Search");
        icon-name: 'system-search-symbolic';
      }

      styles [
        "titlebar",
      ]
    }

    [top]
    SearchBar _search_bar {
      search-mode-enabled: bind _search_button.active bidirectional;

      accessibility {
        label: _("Search passwords");
        checked: true;
      }

      SearchEntry _search_entry {
        placeholder-text: _("Search passwords");

        accessibility {
          labelled-by: _search_bar;
        }

      }
    }

    content: Stack _stack {
      StackPage {
        name: 'empty';

        child: Adw.StatusPage {
          vexpand: true;
          icon-name: 'com.gitlab.guillermop.MasterKey-symbolic';
          title: _("No passwords yet");
          description: _("Add a new password or restore passwords from a file");

          Button {
            label: _("Add password");
            action-name: 'win.add';
            halign: center;

            styles [
              "pill",
              "suggested-action",
            ]
          }
        };
      }

      StackPage {
        name: 'empty-search';

        child: Adw.StatusPage {
          icon-name: 'system-search-symbolic';
          title: _("No results found");
          description: _("Try a different search");
        };
      }

      StackPage {
        name: 'default';

        child: ScrolledWindow {
          child: Adw.Clamp _clamp {
            margin-top: 18;
            margin-bottom: 18;
            margin-start: 18;
            margin-end: 18;

            $ListBox _list_box {
              valign: start;

              styles [
                "boxed-list-separate",
              ]
            }
          };
        };
      }

    };
  }
}

menu app-menu {
  item {
    label: _("Lock the application");
    action: 'win.lock';
  }

  section {
    submenu {
      label: _("Sorting");

      item {
        label: _("A-Z");
        action: 'win.sort-order';
        target: 'a-z';
      }

      item {
        label: _("Z-A");
        action: 'win.sort-order';
        target: 'z-a';
      }

      item {
        label: _("Oldest First");
        action: 'win.sort-order';
        target: 'oldest_first';
      }

      item {
        label: _("Newest First");
        action: 'win.sort-order';
        target: 'newest_first';
      }
    }
  }

  section {
    item {
      label: _("Reset password");
      action: "app.reset";
    }
  }

  section {
    item {
      label: _("Preferences");
      action: 'app.preferences';
    }

    item {
      label: _("Keyboard Shortcuts");
      action: 'win.show-help-overlay';
    }

    item {
      label: _("About Master Key");
      action: 'app.about';
    }
  }
}
