# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import string
from functools import wraps
from random import choice, seed, shuffle
from time import perf_counter

from master_key.crypto import derive_key


def benchmark(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = perf_counter()
        result = func(*args, **kwargs)
        end = perf_counter()
        logging.debug("time taken %s s", end - start)
        return result

    return wrapper


@benchmark
def generate(
    passphrase: str,
    domain: str,
    username: str,
    length: int,
    version: int,
    lowercase: bool,
    uppercase: bool,
    digits: bool,
    symbols: bool,
) -> str:
    characters = {
        "a": string.ascii_lowercase,
        "A": string.ascii_uppercase,
        "d": string.digits,
        "s": string.punctuation,
    }

    if not lowercase:
        del characters["a"]
    if not uppercase:
        del characters["A"]
    if not digits:
        del characters["d"]
    if not symbols:
        del characters["s"]

    default = "".join(characters.values())

    salt = f"{domain}\0{username}\0{version * 1_000}"

    dk = derive_key(passphrase, salt)

    seed(dk, version=2)

    template = list(characters)

    template += ["x"] * (length - len(template))

    shuffle(template)

    return "".join(choice(characters.get(i, default)) for i in template)
