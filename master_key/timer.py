# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import GLib, GObject

from master_key.models.settings import Settings


class Timer(GObject.GObject):
    __gtype_name__ = "Timer"
    __gsignals__ = {"timeout": (GObject.SIGNAL_RUN_FIRST, None, ())}

    _timeout_id: int = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def stop(self):
        if self._timeout_id == 0:
            return
        GLib.Source.remove(self._timeout_id)
        self._timeout_id = 0

    def reset(self):
        self.stop()
        self.start()

    def start(self, *args):
        def timeout(*_):
            self._timeout_id = 0
            self.emit("timeout")
            return False

        minutes = Settings.get_default().lock_timeout * 60
        self._timeout_id = GLib.timeout_add_seconds(minutes, timeout)
