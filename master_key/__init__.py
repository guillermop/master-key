# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later


class SqlcipherError(Exception):
    pass
