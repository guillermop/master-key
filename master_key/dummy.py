# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later


def _(message):
    pass


# translatable strings from zxcvbn
_("Use a few words, avoid common phrases.")
_("No need for symbols, digits, or uppercase letters.")
_("Add another word or two. Uncommon words are better.")
_("Use a longer keyboard pattern with more turns.")
_("Avoid repeated words and characters.")
_("Avoid sequences.")
_("Avoid recent years.")
_("Avoid years that are associated with you.")
_("Avoid dates and years that are associated with you.")
_("Capitalization doesn't help very much.")
_("All-uppercase is almost as easy to guess as all-lowercase.")
_("Reversed words aren't much harder to guess.")
