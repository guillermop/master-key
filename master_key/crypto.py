import base64
import secrets
import string
from hashlib import pbkdf2_hmac

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad


def encrypt(key: bytes, raw: bytes) -> bytes:
    raw = pad(raw, AES.block_size)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw))


def decrypt(key: bytes, encrypted: bytes) -> bytes:
    encrypted = base64.b64decode(encrypted)
    iv, data = encrypted[: AES.block_size], encrypted[AES.block_size :]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(data), AES.block_size)


def create_salt(length: int = 24) -> str:
    salt_chars = string.ascii_letters + string.digits + "./"
    return "".join(secrets.choice(salt_chars) for _ in range(length))


def derive_key(
    password: str,
    salt: str,
    iterations: int = 200_000,
    dklen: int = 32,
) -> bytes:
    return pbkdf2_hmac(
        "sha512", password.encode("utf-8"), salt.encode("utf-8"), iterations, dklen
    )
