# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import sqlite3
from collections.abc import Callable
from enum import IntEnum
from os import listdir
from pathlib import Path
from re import findall
from threading import Thread
from typing import Any, Self

from gi.repository import GLib, GObject

from master_key import SqlcipherError

DB_DIR = Path(GLib.get_user_data_dir()) / "master-key"
MIGRATIONS_DIR = Path(__file__).parent / "migrations"

CREATE_MIGRATION_TABLE = (
    'CREATE TABLE IF NOT EXISTS "migration" ("version" TEXT NOT NULL)'
)
APPLIED_MIGRATIONS = 'SELECT * FROM "migration"'
APPLY_MIGRATION = 'INSERT INTO "migration" VALUES (?)'


def get_cipher_version() -> int:
    temp = sqlite3.connect(":memory:")
    cursor = temp.cursor()
    cursor.execute("PRAGMA cipher_version")
    version = cursor.fetchone()
    if not version:
        raise SqlcipherError
    temp.close()
    return int(version[0][0])


class DatabaseState(IntEnum):
    NOT_CREATED = 1
    LOCKED = 2
    UNLOCKED = 3


class Database(GObject.GObject):
    __gtype_name__ = "Database"

    _state = DatabaseState.LOCKED
    _compat = False
    _instance: Self | None = None
    _connection: sqlite3.Connection
    _cursor: sqlite3.Cursor

    def __init__(self):
        super().__init__()
        self._passphrase = ""
        self._ensure_dirs()
        self._cipher_version = get_cipher_version()
        self._database_file = self._get_db_file()

    @classmethod
    def get_default(cls) -> Self:
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def _ensure_dirs(self):
        DB_DIR.mkdir(parents=True, exist_ok=True)

    def _get_db_file(self) -> Path:
        try:
            f = next(DB_DIR.glob("database*.db"))
            cipher = findall(r"database(\d+)", f.name)[0]
            self._compat = int(cipher) < self._cipher_version
        except StopIteration:
            self._state = DatabaseState.NOT_CREATED
            self.notify("state")
            return DB_DIR / f"database{self._cipher_version}.db"
        return f

    def unlock(self, passphrase: str, callback: Callable[[bool], None]) -> None:
        def _callback(success: bool) -> None:
            if success:
                self._state = DatabaseState.UNLOCKED
                self.notify("state")
            callback(success)

        def target():
            result = None
            try:
                result = self._check_passphrase(passphrase)
            except Exception:
                logging.exception("Failed to open database")

            def idle_safe_callback(success):
                _callback(success)
                return False

            GLib.idle_add(idle_safe_callback, result)

        t = Thread(target=target, daemon=True)
        t.start()

    def _check_passphrase(self, passphrase):
        connection = sqlite3.connect(
            self._database_file,
            check_same_thread=False,
            detect_types=sqlite3.PARSE_DECLTYPES,
        )
        connection.execute(f'PRAGMA key="{passphrase}"')
        if self._compat:
            connection.execute("PRAGMA cipher_compatibility=3")
        try:
            connection.execute("SELECT COUNT(*) FROM sqlite_master")
        except sqlite3.DatabaseError:
            return False

        if self._compat:
            connection.close()

            self._database_file.rename(f"database{self._cipher_version}.db")

            connection = sqlite3.connect(self._database_file, check_same_thread=False)
            connection.execute(f'PRAGMA key="{passphrase}"')
            connection.execute("PRAGMA cipher_migrate")

        self.passphrase = passphrase
        self._connection = connection
        self._connection.row_factory = sqlite3.Row
        self._connection.execute("PRAGMA journal_mode=WAL")
        self._connection.execute("PRAGMA synchronous=NORMAL")
        self._cursor = self._connection.cursor()
        self._apply_migrations()

        return True

    @property
    def passphrase(self) -> str:
        return self._passphrase

    @passphrase.setter
    def passphrase(self, passphrase):
        self._passphrase = passphrase

    @GObject.Property(type=int)
    def state(self):
        return self._state

    def lock(self):
        self.passphrase = ""
        self._state = DatabaseState.LOCKED
        self.notify("state")
        self._cursor.close()
        self._connection.close()

    def reset(self):
        if self._state != DatabaseState.NOT_CREATED:
            self._database_file.unlink()
            self._state = DatabaseState.NOT_CREATED
            self.notify("state")
        if self._connection:
            self._connection.close()

    def bind_locked(self, target: GObject.Object, target_property: str) -> None:
        self.bind_property(
            "state",
            target,
            target_property,
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, state: state == DatabaseState.UNLOCKED,
        )

    def execute(
        self,
        query: str,
        params: list[Any] | dict[str, Any] | None = None,
        commit: bool = False,
    ) -> sqlite3.Cursor:
        if not params:
            params = []
        cursor = self._cursor.execute(query, params)
        if commit:
            self._connection.commit()
        return cursor

    def close(self):
        if self._state == DatabaseState.UNLOCKED:
            self._cursor.close()
            self._connection.close()

    def _apply_migrations(self):
        self.execute(CREATE_MIGRATION_TABLE)
        migrations = [
            f[:-3] for f in sorted(listdir(MIGRATIONS_DIR)) if f.endswith(".py")
        ]
        applied = [
            row["version"] for row in self.execute(APPLIED_MIGRATIONS).fetchall()
        ]
        for version in migrations:
            if version not in applied:
                migration = __import__(
                    f"master_key.migrations.{version}",
                    fromlist=["up"],
                )
                migration.up(self._connection)
                self.execute(APPLY_MIGRATION, [version], True)
