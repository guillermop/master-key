# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Self, TypedDict, Unpack

from gi.repository import GObject

from master_key.database import Database
from master_key.password_generator import generate
from master_key.utils import run_async


class NewPassword(TypedDict):
    domain: str
    username: str
    length: int
    lowercase: bool
    uppercase: bool
    digits: bool
    symbols: bool
    version: int


class PasswordProperties(NewPassword):
    id: int


class UpdatePassword(NewPassword, total=False):
    pass


class Password(GObject.GObject):
    __gtype_name__ = "Password"

    _password = ""
    _generating = False

    id = GObject.Property(type=int)
    domain = GObject.Property(type=str)
    username = GObject.Property(type=str)
    length = GObject.Property(type=int)
    lowercase = GObject.Property(type=bool, default=False)
    uppercase = GObject.Property(type=bool, default=False)
    digits = GObject.Property(type=bool, default=False)
    symbols = GObject.Property(type=bool, default=False)
    version = GObject.Property(type=int)

    def __init__(self, **kwargs: Unpack[PasswordProperties]) -> None:
        super().__init__(**kwargs)  # type: ignore

    @GObject.Property(type=bool, default=False)
    def generating(self) -> bool:
        return self._generating

    @GObject.Property(type=str)
    def password(self) -> str:
        return self._password

    def generate(self):
        passphrase = Database.get_default().passphrase

        def callback(password, *args):
            self._password = password
            self._generating = False
            self.notify("password")
            self.notify("generating")

        self._generating = True
        self.notify("generating")

        run_async(
            generate,
            callback,
            passphrase,
            self.props.domain,
            self.props.username,
            self.props.length,
            self.props.version,
            self.props.lowercase,
            self.props.uppercase,
            self.props.digits,
            self.props.symbols,
        )

    def to_json(self) -> dict:
        return {
            "domain": self.props.domain,
            "username": self.props.username,
            "lowercase": self.props.lowercase,
            "uppercase": self.props.uppercase,
            "digits": self.props.digits,
            "symbols": self.props.symbols,
            "length": self.props.length,
            "version": self.props.version,
        }

    @classmethod
    def select(cls) -> list[Self]:
        db = Database.get_default()
        q = "SELECT * FROM passwords"
        return [cls(**row) for row in db.execute(q).fetchall()]

    @classmethod
    def create(cls, **kwargs: Unpack[NewPassword]) -> Self | None:
        query = "INSERT INTO passwords (id,"
        query += ",".join(kwargs.keys())
        query += ") VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?)"

        db = Database.get_default()
        if last_id := db.execute(query, [*kwargs.values()], True).lastrowid:
            return cls(id=last_id, **kwargs)
        return None

    def update(self, **kwargs: Unpack[UpdatePassword]) -> None:
        if not (set_ := {k: v for (k, v) in kwargs.items() if v != getattr(self, k)}):
            return

        db = Database.get_default()

        query = "UPDATE passwords SET "
        query += ", ".join([f"{col} = ?" for col in set_])
        query += " WHERE id = ?"

        if db.execute(query, [*set_.values(), self.props.id], True).rowcount:
            for k, v in set_.items():
                setattr(self, k, v)
            self.generate()

    def delete(self):
        db = Database.get_default()
        q = "DELETE FROM passwords WHERE id=?"
        db.execute(q, [self.props.id], True)

    def __repr__(self) -> str:
        return (
            f"Password(id={self.props.id}, "
            f"domain={self.props.domain!r}, "
            f"username={self.props.username!r}, "
            f"version={self.props.version}, "
        )
