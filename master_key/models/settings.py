# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Self

from gi.repository import Gio, GObject

from master_key.define import APP_ID


class Settings(Gio.Settings):
    _instance: Self | None = None

    def __init__(self):
        super().__init__(schema_id=APP_ID)

    @classmethod
    def get_default(cls) -> Self:
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    @GObject.Property(type=int)
    def clipboard_timeout(self) -> int:
        return self.get_int("clipboard-timeout")

    @GObject.Property(type=int)
    def lock_timeout(self) -> int:
        return self.get_int("lock-timeout")

    @GObject.Property(type=int)
    def window_width(self) -> int:  # type: ignore
        return self.get_int("window-width")

    @window_width.setter  # type: ignore
    def window_width(self, width: int):
        return self.set_int("window-width", width)

    @GObject.Property(type=int)
    def window_height(self) -> int:  # type: ignore
        return self.get_int("window-height")

    @window_height.setter  # type: ignore
    def window_height(self, height: int):
        return self.set_int("window-height", height)

    @GObject.Property(type=int)
    def window_maximized(self) -> bool:  # type: ignore
        return self.get_boolean("window-maximized")

    @window_maximized.setter  # type: ignore
    def window_maximized(self, maximized: bool):
        return self.set_boolean("window-maximized", maximized)

    @GObject.Property(type=int)
    def sort_order(self) -> int:  # type: ignore
        return self.get_enum("sort-order")

    @GObject.Property(type=bool, default=True)
    def lock_on_session_lock(self) -> bool:  # type: ignore
        return self.get_boolean("lock-on-session-lock")

    @lock_on_session_lock.setter  # type: ignore
    def lock_on_session_lock(self, lock: bool):
        return self.set_boolean("lock-on-session-lock", lock)
