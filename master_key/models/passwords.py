# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from enum import IntEnum
from typing import Any

from gi.repository import Gio, GObject, Gtk

from master_key.models.password import Password
from master_key.models.settings import Settings


class SortOrder(IntEnum):
    AZ = 0
    ZA = 1
    OLDEST = 2
    NEWEST = 3


class Passwords(Gtk.FilterListModel):
    __gtype_name__ = "Passwords"

    _empty = True
    _empty_search = False
    _filter_text = ""

    def __init__(self):
        super().__init__()
        self._model = Gio.ListStore.new(item_type=Password)
        self._trash: list[Password] = []
        self._setup()

    def _setup(self):
        self._sorter = Gtk.CustomSorter()
        self._sorter.set_sort_func(self._sort_func)

        self.set_model(Gtk.SortListModel.new(model=self._model, sorter=self._sorter))

        self._filter = Gtk.CustomFilter()
        self._filter.set_filter_func(self._filter_func)

        self.set_filter(self._filter)

        self.connect(
            "notify::filter-text",
            lambda *_: self._filter.changed(Gtk.FilterChange.DIFFERENT),
        )

        def on_n_items_notify(*args):
            if (empty := self.props.n_items == 0) != self._empty_search:
                self._empty_search = empty
                self.notify("empty-search")

        def on_model_n_items_notify(*args):
            if (empty := self._model.props.n_items == 0) != self._empty:
                self._empty = empty
                self.notify("empty")

        self.connect("notify::n-items", on_n_items_notify)
        self._model.connect("notify::n-items", on_model_n_items_notify)

    def _sort_func(self, a: Password, b: Password, *args: Any) -> int:
        sort_order = Settings.get_default().sort_order
        if sort_order in (SortOrder.AZ, SortOrder.ZA):
            x = a.domain.lower()
            y = b.domain.lower()
            desc = sort_order == SortOrder.ZA
        else:
            x = a.id
            y = b.id
            desc = sort_order == SortOrder.NEWEST
        return (-1 if x < y else 1) * (-1 if desc else 1)

    def _filter_func(self, password: Password) -> bool:
        text = self.filter_text.strip().lower()
        return (
            not text or text in password.props.domain or text in password.props.username
        )

    @GObject.Property(type=bool, default=True)
    def empty(self) -> int:  # type: ignore
        return self._empty

    @GObject.Property(type=bool, default=False)
    def empty_search(self) -> int:  # type: ignore
        return self._empty_search

    @GObject.Property(type=str)
    def filter_text(self) -> str:  # type: ignore
        return self._filter_text

    @filter_text.setter  # type: ignore
    def filter_text(self, text: str):
        self._filter_text = text

    @property
    def trash_n_items(self) -> int:
        return len(self._trash)

    def find(self, password: Password) -> tuple[bool, int]:
        for i, p in enumerate(self):
            if p == password:
                return (True, i)
        return (False, -1)

    def delete(self, *passwords: Password) -> None:
        for p in passwords:
            _, position = self._model.find(p)
            self._model.remove(position)

        self._trash.extend(passwords)

    def load(self):
        self.splice(0, self._model.props.n_items, Password.select())

    def insert(self, *passwords: Password) -> None:
        self.splice(0, 0, list(passwords))

    def splice(self, position: int, n_removals: int, passwords: list[Password]) -> None:
        self._model.splice(position, n_removals, passwords)

    def restore_trash(self):
        self.insert(*self._trash)
        self._trash.clear()

    def empty_trash(self):
        for p in self._trash:
            p.delete()
        self._trash.clear()

    def clear(self):
        self._model.remove_all()

    def resort(self):
        self._sorter.changed(Gtk.SorterChange.DIFFERENT)

    def bind_empty(self, target, target_property):
        flags = GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE
        self.bind_property("empty", target, target_property, flags)
