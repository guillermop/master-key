# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject

from master_key.models.password import Password
from master_key.models.passwords import Passwords
from master_key.models.settings import Settings


def ensure_types():
    GObject.type_ensure(Password)
    GObject.type_ensure(Passwords)
    GObject.type_ensure(Settings)


ensure_types()
