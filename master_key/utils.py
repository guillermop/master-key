# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from collections.abc import Callable
from threading import Thread
from typing import Any

from gi.repository import GLib


def run_async(
    func: Callable[..., Any],
    callback: Callable[..., Any] | None,
    *args: Any,
    **kwargs: Any,
) -> None:
    def target():
        result = None
        error = None

        try:
            result = func(*args, **kwargs)
        except Exception as e:
            error = e

        if callback:
            GLib.idle_add(callback, result, error)

    t = Thread(target=target, daemon=True)
    t.start()
