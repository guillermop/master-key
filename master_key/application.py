# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from collections.abc import Callable
from gettext import gettext as _

from gi.repository import Adw, Gio, GLib, Gtk

from master_key.database import Database
from master_key.define import APP_ID, DEVEL, VERSION
from master_key.widgets.preferences import Preferences
from master_key.widgets.reset_dialog import ResetDialog
from master_key.widgets.window import Window


class Application(Adw.Application):
    def __init__(self):
        super().__init__(
            application_id=APP_ID,
            flags=Gio.ApplicationFlags.FLAGS_NONE,
            resource_base_path="/com/gitlab/guillermop/MasterKey",
            register_session=True,
        )

    def do_startup(self, *args, **kwargs):
        Adw.Application.do_startup(self)
        GLib.set_application_name(_("Master Key"))
        GLib.set_prgname(APP_ID)
        self.__setup_actions()

    def __setup_actions(self):
        self._add_action("quit", self._on_quit, False)
        self._add_action("about", self._on_about)
        self._add_action("preferences", self._on_preferences)
        self._add_action("reset", self._on_reset)

        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])
        self.set_accels_for_action("app.preferences", ["<Ctrl>comma"])
        self.set_accels_for_action("win.lock", ["<Ctrl>L"])
        self.set_accels_for_action("win.add", ["<Ctrl>N"])
        self.set_accels_for_action("win.search", ["<Ctrl>F"])

    def _add_action(
        self, key: str, callback: Callable[..., None], bind: bool = True
    ) -> None:
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        if bind:
            Database.get_default().bind_locked(action, "enabled")
        self.add_action(action)

    def _on_preferences(self, *args):
        if self.props.active_window.props.visible_dialog:  # type: ignore
            return
        preferences = Preferences()
        preferences.present(self.props.active_window)

    def do_activate(self, *args, **kwargs):
        win = self.props.active_window
        if not win:
            win = Window(application=self)
            if DEVEL:
                win.get_style_context().add_class("devel")
        win.present()

    def do_handle_local_options(self, options: GLib.VariantDict) -> int:
        parsed = options.end().unpack()

        loglevel = logging.ERROR
        if DEVEL or "debug" in parsed:
            loglevel = logging.DEBUG

        logging.basicConfig(
            format="[%(levelname)-s] %(asctime)s %(message)s",
            level=loglevel,
        )
        return -1

    def _on_about(self, *args):
        about = Adw.AboutDialog()
        about.set_application_name(_("Master Key"))
        about.set_comments(_("A password manager"))
        about.set_application_icon(APP_ID)
        about.set_developer_name("Guillermo Peña")
        about.set_copyright("© 2022 Guillermo Peña")
        about.set_version(VERSION)
        about.set_issue_url("https://gitlab.com/guillermop/master-key/-/issues/new")
        about.set_license_type(Gtk.License.GPL_3_0)
        about.set_website("https://gitlab.com/guillermop/master-key")
        about.set_translator_credits(_("translator-credits"))
        about.present(self.props.active_window)

    def _on_reset(self, *args):
        dialog = ResetDialog()

        def on_response(_dialog: Adw.Dialog, response: str) -> None:
            if response == "continue":
                Database.get_default().reset()

        dialog.connect("response", on_response)
        dialog.present(self.props.active_window)

    def _on_quit(self, *args):
        if self.props.active_window:
            self.props.active_window.close()
        self.quit()
