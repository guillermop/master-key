# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from typing import cast

from gi.repository import Adw, GObject, Gtk
from zxcvbn import zxcvbn

from master_key.database import Database
from master_key.define import RES_PATH

MIN_SCORE = 4


@Gtk.Template(resource_path=f"{RES_PATH}/ui/setup-view.ui")
class SetupView(Adw.Bin):
    __gtype_name__ = "SetupView"

    confirmed = GObject.Property(type=bool, default=True)

    _password_entry = cast(Adw.PasswordEntryRow, Gtk.Template.Child())
    _setup_button = cast(Gtk.Button, Gtk.Template.Child())
    _confirm_entry = cast(Adw.PasswordEntryRow, Gtk.Template.Child())
    _error_revealer = cast(Gtk.Revealer, Gtk.Template.Child())
    _error_label = cast(Gtk.Label, Gtk.Template.Child())
    _level_bar = cast(Gtk.LevelBar, Gtk.Template.Child())
    _feedback_revealer = cast(Gtk.Revealer, Gtk.Template.Child())
    _feedback_label = cast(Gtk.Label, Gtk.Template.Child())

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._add_offsets()

    def do_grab_focus(self) -> bool:
        return self._password_entry.grab_focus()

    def _add_offsets(self):
        self._level_bar.add_offset_value("weak", 1)
        self._level_bar.add_offset_value("low", 2)
        self._level_bar.add_offset_value("medium", 3)
        self._level_bar.add_offset_value("good", 4)
        self._level_bar.add_offset_value("high", 5)

    def _clear_widgets(self, *_):
        self._password_entry.set_text("")
        self._confirm_entry.set_text("")
        self._hide_error()
        self._hide_feedback()
        self.set_sensitive(True)

    @Gtk.Template.Callback()
    def _on_setup(self, *args):
        if self.confirmed:
            self._setup_button.grab_focus()
            self.set_sensitive(False)

            def on_response_cb(success):
                if success:
                    self._clear_widgets()
                else:
                    self.set_sensitive(True)
                    self._password_entry.grab_focus()

            Database.get_default().unlock(
                self._password_entry.get_text(),
                on_response_cb,
            )

    @Gtk.Template.Callback()
    def _on_passwords_changed(self, *args):
        if self._level_bar.get_value() < MIN_SCORE:
            self.props.confirmed = False
            return

        if not self._confirm_entry.get_text():
            self.props.confirmed = False
            self._hide_error()
            return

        if self._confirm_entry.get_text() != self._password_entry.get_text():
            self.props.confirmed = False
            self._show_error(_("Password doesn't match"))
        else:
            self._hide_error()
            self.props.confirmed = True

    @Gtk.Template.Callback()
    def _on_password_entry_changed(self, entry: Adw.PasswordEntryRow) -> None:
        password = entry.get_text()
        score = 0
        if password:
            results = zxcvbn(password)
            score = results.get("score", 0) + 1
            feedback = results.get("feedback")
            if score >= MIN_SCORE:
                self._hide_feedback()
            else:
                suggestions = [_(suggestion) for suggestion in feedback["suggestions"]]
                self._show_feedback(" ".join(suggestions))
        else:
            self._hide_feedback()
        self._level_bar.set_value(score)

    def _show_feedback(self, feedback: str) -> None:
        self._feedback_revealer.set_reveal_child(True)
        self._feedback_label.set_label(feedback)
        self._password_entry.get_style_context().add_class("error")

    def _hide_feedback(self):
        self._feedback_revealer.set_reveal_child(False)
        self._password_entry.get_style_context().remove_class("error")

    def _show_error(self, error: str) -> None:
        self._error_revealer.set_reveal_child(True)
        self._error_label.set_label(error)
        self._confirm_entry.get_style_context().add_class("error")

    def _hide_error(self):
        self._error_revealer.set_reveal_child(False)
        self._error_label.set_label("")
        self._confirm_entry.get_style_context().remove_class("error")
