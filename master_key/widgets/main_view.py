# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import TYPE_CHECKING, Any, cast

from gi.repository import Adw, GObject, Gtk

from master_key.define import RES_PATH
from master_key.widgets.row import Row

if TYPE_CHECKING:
    from master_key.models.passwords import Passwords
    from master_key.widgets.list_box import ListBox
    from master_key.widgets.window import Window


@Gtk.Template(resource_path=f"{RES_PATH}/ui/main-view.ui")
class MainView(Adw.Bin):
    __gtype_name__ = "MainView"

    _stack = cast(Gtk.Stack, Gtk.Template.Child())
    _list_box = cast("ListBox", Gtk.Template.Child())
    _search_entry = cast(Gtk.SearchEntry, Gtk.Template.Child())
    _search_button = cast(Gtk.Button, Gtk.Template.Child())
    _search_bar = cast(Gtk.SearchBar, Gtk.Template.Child())
    _active_row: Row | None = None

    def bind_model(self, passwords: "Passwords") -> None:
        passwords.connect("notify::empty", self._on_empty_notify)
        passwords.connect("notify::empty-search", self._on_empty_notify)
        passwords.bind_property(
            "empty",
            self._search_button,
            "sensitive",
            GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE,
        )
        self._search_entry.bind_property(
            "text",
            passwords,
            "filter-text",
            GObject.BindingFlags.SYNC_CREATE,
        )
        self._list_box.bind_model(passwords, Row)

    def toggle_search(self):
        mode = self._search_bar.props.search_mode_enabled
        if mode and not self._search_bar.get_focus_child():
            self._search_entry.grab_focus()
        else:
            self._search_bar.props.search_mode_enabled = not mode

    def _on_empty_notify(self, passwords: "Passwords", *args: Any) -> None:
        self._search_bar.set_key_capture_widget(None)
        if passwords.empty:
            self._stack.props.visible_child_name = "empty"
            self._search_bar.props.search_mode_enabled = False
        elif passwords.empty_search:
            self._stack.props.visible_child_name = "empty-search"
            return
        else:
            self._stack.props.visible_child_name = "default"
            win = cast("Window", self.props.root)
            self._search_bar.set_key_capture_widget(win)

    def activate_row(self, position: int) -> None:
        if row := self._list_box.get_row_at_index(position):
            row.activate()

    def _on_empty_search_changed(self, model, *args):
        child = "empty" if model.props.empty_search else "list"
        self._stack.props.visible_child_name = child
