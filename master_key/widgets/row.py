# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from collections.abc import Callable
from gettext import gettext as _
from typing import TYPE_CHECKING, Any, cast

from gi.repository import Adw, Gio, GObject, Gtk

from master_key.define import RES_PATH
from master_key.models.password import Password
from master_key.widgets.password_row import PasswordRow

if TYPE_CHECKING:
    from master_key.widgets.list_box import ListBox
    from master_key.widgets.window import Window


@Gtk.Template(resource_path=f"{RES_PATH}/ui/row.ui")
class Row(Adw.ExpanderRow):
    __gtype_name__ = "Row"

    _password_row = cast(PasswordRow, Gtk.Template.Child())
    _version_label = cast(Gtk.Label, Gtk.Template.Child())

    _password: Password

    def __init__(self, password: Password):
        super().__init__()
        self._password = password
        self._setup()
        self.notify("password")

    @GObject.Property(type=Password)
    def password(self):
        return self._password

    def _setup(self):
        self._signals = []
        self._signal_connect(self, "notify::parent", self._on_parent_notify)
        self._signal_connect(self, "notify::expanded", self._on_expanded_changed)
        self._signal_connect(self._password_row, "copy", self._on_copy_password)

        self._password.bind_property(
            "version",
            self._version_label,
            "label",
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, version: _("version: {0}").format(version),
        )

        self.__action_group = Gio.SimpleActionGroup()

        self._add_action("copy-password", self._on_copy_password)
        self._add_action("copy-username", self._on_copy_username)
        self._add_action("edit", self._on_edit)
        self._add_action("delete", self._on_delete)
        self._add_action("collapse", self._on_collapse)

        self.insert_action_group("row", self.__action_group)

    def _add_action(self, key, callback):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        self.__action_group.add_action(action)
        self.bind_property(
            "expanded",
            action,
            "enabled",
            GObject.BindingFlags.DEFAULT | GObject.BindingFlags.SYNC_CREATE,
        )

    def _on_copy_password(self, *args):
        password = self._password_row.props.text
        if not password:
            return

        window = cast("Window", self.get_root())
        window.copy_to_clipboard(password)
        window.add_toast(Adw.Toast(title=_("Password copied")))

    def _on_copy_username(self, *args):
        window = cast("Window", self.get_root())
        window.copy_to_clipboard(self._password.username)
        window.add_toast(Adw.Toast(title=_("Username copied")))

    def _on_edit(self, *args):
        window = cast("Window", self.get_root())
        window.edit_password(self._password)

    def _on_delete(self, *args):
        parent = cast("ListBox", self.props.parent)
        parent.active_row = None
        window = cast("Window", self.get_root())
        window.delete_password(self._password)

    def _on_collapse(self, *args):
        self.props.expanded = False

    def _on_expanded_changed(self, *args):
        if self.props.expanded:
            parent = cast("ListBox", self.props.parent)
            parent.active_row = self
            if not self._password_row.props.text:
                self._password.generate()

    def _on_parent_notify(self, *args):
        if not self.props.parent:
            for obj, signal in self._signals:
                obj.disconnect(signal)
            self._signals.clear()
            self.__action_group.remove_action("copy-password")
            self.__action_group.remove_action("copy-username")
            self.__action_group.remove_action("edit")
            self.__action_group.remove_action("delete")
            self.__action_group.remove_action("collapse")

    def _signal_connect(
        self,
        obj: GObject.Object,
        name: str,
        handler: Callable[..., Any],
    ) -> None:
        signal = GObject.signal_connect_closure(obj, name, handler, False)
        self._signals.append((obj, signal))
