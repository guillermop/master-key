# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from typing import TYPE_CHECKING, Any, cast

from gi.repository import Adw, Gio, GObject, Gtk

from master_key.clipboard import Clipboard
from master_key.database import Database, DatabaseState
from master_key.define import DEVEL, RES_PATH
from master_key.models.password import Password
from master_key.models.passwords import Passwords
from master_key.models.settings import Settings
from master_key.timer import Timer
from master_key.widgets.dialog import Dialog
from master_key.widgets.main_view import MainView
from master_key.widgets.setup_view import SetupView
from master_key.widgets.unlock_view import UnlockView

if TYPE_CHECKING:
    from master_key.application import Application


@Gtk.Template(resource_path=f"{RES_PATH}/ui/window.ui")
class Window(Adw.ApplicationWindow):
    __gtype_name__ = "Window"

    _overlay = cast(Adw.ToastOverlay, Gtk.Template.Child())
    _stack = cast(Gtk.Stack, Gtk.Template.Child())
    _unlock_view = cast(UnlockView, Gtk.Template.Child())
    _setup_view = cast(SetupView, Gtk.Template.Child())
    _main_view = cast(MainView, Gtk.Template.Child())

    _notification: Adw.Toast | None = None

    _screensaver_active_id: int | None = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._clipboard_manager = Clipboard(self)
        self._timer = Timer()
        self._timer.connect_after("timeout", self._on_lock)
        self._passwords = Passwords()
        self._main_view.bind_model(self._passwords)
        self._setup_database()
        self._setup_actions()
        self._load_settings()

    def do_enable_debugging(self, toggle: bool) -> bool:
        if not DEVEL:
            return False
        return Adw.ApplicationWindow.do_enable_debugging(self, toggle)

    def _setup_database(self):
        database = Database.get_default()
        database.connect("notify::state", self._on_db_state_notify)
        self._on_db_state_notify(database)

    def _on_db_state_notify(self, database: Database, *args: Any) -> None:
        if database.state == DatabaseState.LOCKED:
            self._stack.props.visible_child_name = "unlock"
            self._unlock_view.grab_focus()
            self._timer.stop()
        elif database.state == DatabaseState.NOT_CREATED:
            self._stack.props.visible_child_name = "setup"
            self._setup_view.grab_focus()
            self._timer.stop()
        else:
            self._stack.props.visible_child_name = "main"
            self._timer.start()
            self._passwords.load()

    def _setup_actions(self):
        self._add_action("add", self._on_add_password)
        self._add_action("search", self._on_search, bind_empty=True)
        self._add_action("lock", self._on_lock)

        settings = Settings.get_default()
        sort_action = settings.create_action("sort-order")
        self.add_action(sort_action)

    def _add_action(self, key, callback, bind_empty=False):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        if bind_empty:
            self._passwords.bind_empty(action, "enabled")
        else:
            Database.get_default().bind_locked(action, "enabled")
        self.add_action(action)

    def _load_settings(self):
        settings = Settings.get_default()
        settings.connect("changed::sort-order", lambda *_: self._passwords.resort())

        app = cast("Application", self.props.application)
        self._screensaver_active_id = app.connect(
            "notify::screensaver-active",
            self._on_screensaver_active_notify,
        )

        self.set_default_size(settings.window_width, settings.window_height)
        if settings.window_maximized:
            self.maximize()

    def _on_screensaver_active_notify(self, app: "Application", *args: Any) -> None:
        if (
            app.props.screensaver_active
            and Database.get_default().state == DatabaseState.UNLOCKED
            and Settings.get_default().lock_on_session_lock
        ):
            self._on_lock()

    def _on_add_password(self, *args):
        self._show_password_dialog()

    def edit_password(self, password: Password) -> None:
        self._show_password_dialog(password)

    def _on_search(self, *args):
        self._main_view.toggle_search()

    def _on_lock(self, *args):
        if dialog := self.props.visible_dialog:
            dialog.force_close()
        Database.get_default().lock()

    def delete_password(self, password: Password) -> None:
        self._passwords.delete(password)

        if self._passwords.trash_n_items > 1:
            message = _("{0} passwords deleted.").format(self._passwords.trash_n_items)
        else:
            message = _('Password "{0}" deleted.').format(password.props.domain)

        if self._notification:
            self._notification.set_title(message)
            return

        self._notification = Adw.Toast(
            title=message,
            button_label=_("Undo"),
            priority=Adw.ToastPriority.HIGH,
        )

        def on_dismissed(*args):
            self._notification = None
            self._passwords.empty_trash()

        def on_undo(*args):
            self._passwords.restore_trash()

        self._notification.connect("dismissed", on_dismissed)
        self._notification.connect("button-clicked", on_undo)
        self._overlay.add_toast(self._notification)

    @Gtk.Template.Callback()
    def _on_close_request(self, *args):
        if self._notification:
            self._notification.dismiss()
        settings = Settings.get_default()
        width, height = self.get_default_size()
        settings.props.window_width = width
        settings.props.window_height = height
        settings.props.window_maximized = self.is_maximized()

        if self._screensaver_active_id:
            app = cast("Application", self.props.application)
            app.disconnect(self._screensaver_active_id)
            self._screensaver_active_id = None

        Database.get_default().close()

        return False

    @Gtk.Template.Callback()
    def _on_clicked(self, *args):
        if Database.get_default().state == DatabaseState.UNLOCKED:
            self._timer.reset()

    @Gtk.Template.Callback()
    def _on_key_pressed(self, *args):
        if Database.get_default().state == DatabaseState.UNLOCKED:
            self._timer.reset()

    def _show_password_dialog(
        self,
        password: Password | None = None,
    ) -> None:
        if self.props.visible_dialog:
            return

        dialog = Dialog(password=password)

        def on_response(_dialog, response):
            if response == Gtk.ResponseType.OK:
                data = dialog.data
                position = None
                if password:
                    resort = (
                        password.props.domain != data["domain"]
                        or password.props.username != data["username"]
                    )
                    password.update(**dialog.data)
                    if resort:
                        self._passwords.resort()
                        _found, position = self._passwords.find(password)
                elif new_password := Password.create(**data):
                    self._passwords.insert(new_password)
                    _found, position = self._passwords.find(new_password)
                if position is not None:
                    self._main_view.activate_row(position)

            dialog.close()

        dialog.connect("response", on_response)
        dialog.present(self)

    def copy_to_clipboard(self, text: str) -> None:
        self._clipboard_manager.copy(text)

    def add_toast(self, toast: Adw.Toast) -> None:
        self._overlay.add_toast(toast)

    @GObject.Property(type=Passwords)
    def passwords(self):
        return self._passwords
