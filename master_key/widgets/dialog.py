# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from typing import TYPE_CHECKING, Any, cast

from gi.repository import Adw, GLib, GObject, Gtk

from master_key.database import Database
from master_key.define import RES_PATH
from master_key.models.password import Password
from master_key.password_generator import generate
from master_key.utils import run_async
from master_key.widgets.password_row import PasswordRow

if TYPE_CHECKING:
    from master_key.widgets.window import Window


def is_not_empty(string: str) -> bool:
    return bool(string and string.strip())


@Gtk.Template(resource_path=f"{RES_PATH}/ui/dialog.ui")
class Dialog(Adw.Dialog):
    __gtype_name__ = "Dialog"

    __gsignals__ = {"response": (GObject.SIGNAL_RUN_FIRST, None, (int,))}

    _overlay = cast(Adw.ToastOverlay, Gtk.Template.Child())
    _domain_entry = cast(Adw.EntryRow, Gtk.Template.Child())
    _username_entry = cast(Adw.EntryRow, Gtk.Template.Child())
    _version_entry = cast(Adw.SpinRow, Gtk.Template.Child())
    _length_entry = cast(Adw.SpinRow, Gtk.Template.Child())
    _lowercase_switch = cast(Adw.SwitchRow, Gtk.Template.Child())
    _uppercase_switch = cast(Adw.SwitchRow, Gtk.Template.Child())
    _digits_switch = cast(Adw.SwitchRow, Gtk.Template.Child())
    _symbols_switch = cast(Adw.SwitchRow, Gtk.Template.Child())
    _password_row = cast(PasswordRow, Gtk.Template.Child())
    _save_button = cast(Gtk.Button, Gtk.Template.Child())

    _timeout_id: int | None = None
    _can_save = False

    def __init__(self, password: Password | None = None, **kwargs: Any):
        super().__init__(**kwargs)
        self._password = password
        self._fields = {
            "_domain_entry": False,
            "_username_entry": False,
        }
        self._init_widgets()

    def _init_widgets(self):
        if not self._password:
            self.set_title(_("Add password"))
            return
        self.set_title(_("Edit password"))
        self._domain_entry.set_text(self._password.props.domain)
        self._username_entry.set_text(self._password.props.username)
        self._length_entry.set_value(self._password.props.length)
        self._version_entry.set_value(self._password.props.version)
        self._uppercase_switch.set_active(self._password.props.uppercase)
        self._lowercase_switch.set_active(self._password.props.lowercase)
        self._digits_switch.set_active(self._password.props.digits)
        self._symbols_switch.set_active(self._password.props.symbols)
        self._domain_entry.grab_focus()

    @GObject.Property(type=bool, default=False)
    def can_save(self):
        return self._can_save

    @property
    def data(self):
        return {
            "domain": self._domain_entry.get_text(),
            "username": self._username_entry.get_text(),
            "length": int(self._length_entry.get_value()),
            "uppercase": self._uppercase_switch.get_active(),
            "lowercase": self._lowercase_switch.get_active(),
            "digits": self._digits_switch.get_active(),
            "symbols": self._symbols_switch.get_active(),
            "version": int(self._version_entry.get_value()),
        }

    @Gtk.Template.Callback()
    def _update_response_buttons(self, entry: Adw.EntryRow) -> None:
        field = entry.get_buildable_id()

        if field not in self._fields:
            return

        valid = is_not_empty(entry.get_text())

        if valid:
            entry.get_style_context().remove_class("error")
        else:
            entry.get_style_context().add_class("error")

        self._fields[field] = valid

        can_save = all(field for field in self._fields.values())

        if self._can_save != can_save:
            if not can_save:
                self._password_row.props.text = ""
                self._password_row.props.loading = False
                self._clear_timeout()
            self._can_save = can_save
            self.notify("can-save")

        self._save_button.props.sensitive = can_save

        self._generate()

    def _generate(self):
        def callback(password: str, *args: Any) -> None:
            if self._can_save:
                self._password_row.props.text = password
                self._password_row.props.loading = False

        def _generate():
            passphrase = Database.get_default().passphrase
            run_async(
                generate,
                callback,
                passphrase,
                self._domain_entry.props.text,
                self._username_entry.props.text,
                int(self._length_entry.props.value),
                int(self._version_entry.props.value),
                self._lowercase_switch.props.active,
                self._uppercase_switch.props.active,
                self._digits_switch.props.active,
                self._symbols_switch.props.active,
            )
            self._timeout_id = None

        if self._can_save:
            self._clear_timeout()
            self._password_row.props.loading = True
            self._timeout_id = GLib.timeout_add(400, _generate)

    @Gtk.Template.Callback()
    def _on_spin_value_notify(self, *args):
        self._generate()

    @Gtk.Template.Callback()
    def _on_switch_active_notify(self, *args):
        rows = [
            r
            for r in [
                self._lowercase_switch,
                self._uppercase_switch,
                self._digits_switch,
                self._symbols_switch,
            ]
            if r.props.active
        ]
        if len(rows) == 1:
            rows[0].props.sensitive = False
        else:
            for row in rows:
                row.props.sensitive = True

        self._generate()

    @Gtk.Template.Callback()
    def _on_save_button_clicked(self, *args):
        if not self._can_save:
            return
        if self._password:
            equal = all(
                getattr(self._password, name) == val for name, val in self.data.items()
            )
            if equal:
                self.emit("response", Gtk.ResponseType.CANCEL)
                return
        self.emit("response", Gtk.ResponseType.OK)

    @Gtk.Template.Callback()
    def _on_password_row_copy(self, password_row: PasswordRow) -> None:
        win = cast("Window", self.props.root)
        win.copy_to_clipboard(password_row.props.text)
        toast = Adw.Toast(title=_("Password copied"))
        self._add_toast(toast)

    @Gtk.Template.Callback()
    def _on_close_button_clicked(self, *args):
        self.emit("response", Gtk.ResponseType.CANCEL)

    def _clear_timeout(self):
        if self._timeout_id:
            GLib.Source.remove(self._timeout_id)
            self._timeout_id = None

    def _add_toast(self, toast: Adw.Toast) -> None:
        self._overlay.add_toast(toast)
