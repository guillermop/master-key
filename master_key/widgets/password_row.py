# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from typing import cast

from gi.repository import Adw, GObject, Gtk

from master_key.define import RES_PATH


@Gtk.Template(resource_path=f"{RES_PATH}/ui/password-row.ui")
class PasswordRow(Adw.PreferencesRow):
    __gtype_name__ = "PasswordRow"

    __gsignals__ = {"copy": (GObject.SIGNAL_RUN_FIRST, None, ())}

    _stack = cast(Gtk.Stack, Gtk.Template.Child())
    _password_entry = cast(Gtk.Entry, Gtk.Template.Child())
    _show_button = cast(Gtk.Button, Gtk.Template.Child())
    _copy_button = cast(Gtk.Button, Gtk.Template.Child())

    text = GObject.Property(type=str)
    loading = GObject.Property(type=bool, default=False)
    visibility = GObject.Property(type=bool, default=False)
    placeholder_text = GObject.Property(type=str)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.bind_property(
            "loading",
            self._stack,
            "visible-child-name",
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, loading: ("spinner" if loading else "text"),
        )
        self._password_entry.bind_property(
            "text",
            self._show_button,
            "sensitive",
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, text: bool(text),
        )
        self._password_entry.bind_property(
            "text",
            self._copy_button,
            "sensitive",
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, text: bool(text),
        )
        self.bind_property(
            "visibility",
            self._show_button,
            "icon-name",
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, visibility: "view-conceal-symbolic"
            if visibility
            else "view-reveal-symbolic",
        )
        self.bind_property(
            "visibility",
            self._show_button,
            "tooltip-text",
            GObject.BindingFlags.SYNC_CREATE,
            transform_to=lambda _binding, visibility: _("Hide password")
            if visibility
            else _("Show password"),
        )
        self._password_entry.set_invisible_char("•")

    @Gtk.Template.Callback()
    def _on_show_button_clicked(self, *args):
        self.props.visibility = not self.props.visibility

    @Gtk.Template.Callback()
    def _on_copy_button_clicked(self, *args):
        self.emit("copy")
