# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import time
from gettext import gettext as _
from gettext import ngettext
from typing import TYPE_CHECKING, cast

from gi.repository import Adw, Gio, GLib, GObject, Gtk

from master_key.backup import backup_passwords, restore_passwords
from master_key.define import RES_PATH
from master_key.models.password import Password
from master_key.models.settings import Settings
from master_key.utils import run_async

if TYPE_CHECKING:
    from master_key.widgets.window import Window


VERSION = 1


@Gtk.Template(resource_path=f"{RES_PATH}/ui/preferences.ui")
class Preferences(Adw.PreferencesDialog):
    __gtype_name__ = "Preferences"

    _seconds_spin_button = cast(Adw.SpinRow, Gtk.Template.Child())
    _minutes_spin_button = cast(Adw.SpinRow, Gtk.Template.Child())
    _lock_switch = cast(Adw.SwitchRow, Gtk.Template.Child())

    _importing = False
    _exporting = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        settings = Settings.get_default()
        settings.bind(
            "clipboard-timeout",
            self._seconds_spin_button,
            "value",
            Gio.SettingsBindFlags.DEFAULT,
        )
        settings.bind(
            "lock-timeout",
            self._minutes_spin_button,
            "value",
            Gio.SettingsBindFlags.DEFAULT,
        )
        settings.bind(
            "lock-on-session-lock",
            self._lock_switch,
            "active",
            Gio.SettingsBindFlags.DEFAULT,
        )

    def _on_export_completed(self, n_habits: int, error: Exception | None) -> None:
        if error is None:
            message = ngettext(
                "{} habit exported",
                "{} habits exported",
                n_habits,
            ).format(n_habits)
            toast = Adw.Toast(title=message)
            self.add_toast(toast)
        else:
            logging.error(error)
        self.props.can_close = True
        self._exporting = False
        self.notify("exporting")

    @Gtk.Template.Callback()
    def _on_export_row_activated(self, *args):
        if self._exporting:
            return

        win = cast("Window", self.props.root)

        file_dialog = Gtk.FileDialog(
            default_filter=Gtk.FileFilter(mime_types=["application/json"]),
            initial_name=f'{time.strftime("%Y%m%d-%H%M%S")}.json',
        )

        def callback(dialog: Gtk.FileDialog, result: Gio.AsyncResult) -> None:
            self._exporting = True
            self.notify("exporting")

            try:
                if file := dialog.save_finish(result):
                    path = file.get_path()

                    run_async(backup_passwords, self._on_export_completed, path)

            except GLib.Error:
                self.props.can_close = True
                self._exporting = False
                self.notify("exporting")

        self.props.can_close = False

        file_dialog.save(win, None, callback)

    def _on_import_completed(
        self, passwords: list[Password], error: Exception | None
    ) -> None:
        if error is None:
            n_passwords = len(passwords)
            message = ngettext(
                "{} password imported",
                "{} passwords imported",
                n_passwords,
            ).format(n_passwords)
            toast = Adw.Toast(title=message)
            self.add_toast(toast)
            win = cast("Window", self.props.root)
            win.props.passwords.insert(*passwords)
        else:
            toast = Adw.Toast(title=_("Failed to import"))
            self.add_toast(toast)
            logging.error(error)
        self.props.can_close = True
        self._importing = False
        self.notify("importing")

    @Gtk.Template.Callback()
    def _on_import_row_activated(self, *args):
        if self._importing:
            return

        win = cast("Window", self.props.root)

        file_dialog = Gtk.FileDialog(
            default_filter=Gtk.FileFilter(mime_types=["application/json"]),
        )

        def callback(dialog: Gtk.FileDialog, result: Gio.AsyncResult) -> None:
            self._importing = True
            self.notify("importing")

            try:
                if file := dialog.open_finish(result):
                    path = file.get_path()

                    run_async(restore_passwords, self._on_import_completed, path)

            except GLib.Error:
                self.props.can_close = True
                self._importing = False
                self.notify("importing")

        self.props.can_close = False

        file_dialog.open(win, None, callback)

    @GObject.Property(type=bool, default=False)
    def importing(self) -> bool:
        return self._importing

    @GObject.Property(type=bool, default=False)
    def exporting(self) -> bool:
        return self._exporting
