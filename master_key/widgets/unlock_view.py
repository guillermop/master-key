# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from typing import cast

from gi.repository import Adw, Gtk

from master_key.database import Database
from master_key.define import RES_PATH


@Gtk.Template(resource_path=f"{RES_PATH}/ui/unlock-view.ui")
class UnlockView(Adw.Bin):
    __gtype_name__ = "UnlockView"

    _box = cast(Gtk.Box, Gtk.Template.Child())
    _password_entry = cast(Adw.PasswordEntryRow, Gtk.Template.Child())
    _unlock_button = cast(Gtk.Button, Gtk.Template.Child())
    _error_revealer = cast(Gtk.Revealer, Gtk.Template.Child())
    _error_label = cast(Gtk.Label, Gtk.Template.Child())

    def do_grab_focus(self) -> bool:
        return self._password_entry.grab_focus()

    @Gtk.Template.Callback()
    def _on_unlock(self, *args):
        password = self._password_entry.get_text()
        if not password:
            return
        self._unlock_button.grab_focus()
        self._box.set_sensitive(False)

        def on_response_cb(success: bool) -> None:
            if success:
                self._clear_widgets()
            else:
                self._show_error_message()

        Database.get_default().unlock(password, on_response_cb)

    def _clear_widgets(self, *_):
        self._password_entry.set_text("")
        self._error_revealer.set_reveal_child(False)
        self._error_label.set_label("")
        self._password_entry.get_style_context().remove_class("error")
        self._box.set_sensitive(True)

    def _show_error_message(self, *args):
        message = _("Failed to unlock database. Try again.")
        self._error_revealer.set_reveal_child(True)
        self._error_label.set_label(message)
        self._password_entry.get_style_context().add_class("error")
        self._box.set_sensitive(True)
        self._password_entry.grab_focus()
