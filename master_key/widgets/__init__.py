# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject

from master_key.widgets.dialog import Dialog
from master_key.widgets.list_box import ListBox
from master_key.widgets.main_view import MainView
from master_key.widgets.password_row import PasswordRow
from master_key.widgets.preferences import Preferences
from master_key.widgets.reset_dialog import ResetDialog
from master_key.widgets.row import Row
from master_key.widgets.setup_view import SetupView
from master_key.widgets.unlock_view import UnlockView
from master_key.widgets.window import Window


def ensure_types():
    GObject.type_ensure(Dialog)
    GObject.type_ensure(ListBox)
    GObject.type_ensure(MainView)
    GObject.type_ensure(PasswordRow)
    GObject.type_ensure(Preferences)
    GObject.type_ensure(ResetDialog)
    GObject.type_ensure(Row)
    GObject.type_ensure(SetupView)
    GObject.type_ensure(UnlockView)
    GObject.type_ensure(Window)


ensure_types()
