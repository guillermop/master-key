# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import Adw, Gtk

from master_key.define import RES_PATH


@Gtk.Template(resource_path=f"{RES_PATH}/ui/reset-dialog.ui")
class ResetDialog(Adw.AlertDialog):
    __gtype_name__ = "ResetDialog"

    @Gtk.Template.Callback()
    def _on_confirm_check_toggled(self, check: Gtk.CheckButton) -> None:
        self.set_response_enabled("continue", check.props.active)
