# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import TYPE_CHECKING, Optional

from gi.repository import Gtk

if TYPE_CHECKING:
    from master_key.widgets.row import Row


class ListBox(Gtk.ListBox):
    __gtype_name__ = "ListBox"

    _active_row = None

    @property
    def active_row(self):
        return self._active_row

    @active_row.setter
    def active_row(self, row: Optional["Row"]) -> None:
        if self._active_row and self._active_row != row:
            self._active_row.props.expanded = False
        self._active_row = row
