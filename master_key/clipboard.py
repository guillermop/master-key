# Copyright 2024 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import TYPE_CHECKING, Any

from gi.repository import GLib

from master_key.models.settings import Settings

if TYPE_CHECKING:
    from master_key.widgets.window import Window


class Clipboard:
    _is_active_id: int = 0

    def __init__(self, window: "Window"):
        self._window = window
        self._clipboard = window.get_clipboard()

    def clear(self):
        self._clipboard.set_content(None)

    def copy(self, text: str) -> None:
        self._clipboard.set(text)

        def on_timeout(*args: Any) -> bool:
            if self._window.props.is_active:
                self.clear()
            else:
                self._is_active_id = self._window.connect(
                    "notify::is-active",
                    self._on_window_is_active_changed,
                    text,
                )
            return GLib.SOURCE_REMOVE

        seconds = Settings.get_default().clipboard_timeout
        GLib.timeout_add_seconds(seconds, on_timeout)

    def _on_window_is_active_changed(self, _window, _is_active, last_text):
        if self._is_active_id:
            self._window.disconnect(self._is_active_id)
            self._is_active_id = 0

        def callback(_, result, *args):
            try:
                text = self._clipboard.read_text_finish(result)
                if last_text == text:
                    self.clear()
            except GLib.Error:
                pass

        self._clipboard.read_text_async(None, callback)
