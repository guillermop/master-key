def up(connection):
    connection.execute(
        """CREATE TABLE IF NOT EXISTS passwords (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            domain TEXT NOT NULL,
            username TEXT NOT NULL,
            lowercase INTEGER NOT NULL,
            uppercase INTEGER NOT NULL,
            digits INTEGER NOT NULL,
            symbols INTEGER NOT NULL,
            length INTEGER NOT NULL,
            version INTEGER NOT NULL
        );""",
    )
