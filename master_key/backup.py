import json
from pathlib import Path

from master_key.crypto import create_salt, decrypt, derive_key, encrypt
from master_key.database import Database
from master_key.models.password import Password

VERSION = 2


class RestoreError(Exception):
    pass


def backup_passwords(path: str) -> int:
    passwords = Password.select()

    salt = create_salt()
    key = derive_key(Database.get_default().passphrase, salt)

    data = json.dumps(
        {
            "version": VERSION,
            "salt": salt,
            "passwords": encrypt(
                key,
                json.dumps(
                    {i: item.to_json() for i, item in enumerate(passwords, 1)}
                ).encode("utf-8"),
            ).decode("utf-8"),
        },
        indent=4,
        ensure_ascii=False,
    )
    with Path(path).open("w", encoding="utf-8") as f:
        f.write(data)

    return len(passwords)


def restore_passwords(path: str) -> list[Password]:
    with Path(path).open(encoding="utf-8") as f:
        data = json.load(f)

        if any(k not in data for k in ("version", "passwords")):
            msg = "Invalid data"
            raise RestoreError(msg)

        if data["version"] == VERSION:
            if "salt" not in data:
                msg = "Invalid data"
                raise RestoreError(msg)
            try:
                key = derive_key(Database.get_default().passphrase, data["salt"])
                decrypted_passwords = decrypt(key, data["passwords"].encode("utf-8"))
                passwords = json.loads(decrypted_passwords)
            except Exception:
                msg = "Decryption failed"
                raise RestoreError(msg) from None
        else:
            passwords = data["passwords"]
            if not isinstance(passwords, dict):
                msg = "Invalid data"
                raise RestoreError(msg)

        return [
            p
            for _index, password in passwords.items()
            if (p := Password.create(**password))
        ]
