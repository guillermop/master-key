#!/bin/bash

outdir=""

show_help()
{
   echo "Usage: $0 [options] <migration name>"
   echo "Create a migration file."
   echo
   echo "Options:"
   echo "-o     set output directory"
   echo "-h     display this help and exit"
   echo
}

while getopts ":o:h" opt; do
  case ${opt} in
    o)
      outdir="${OPTARG}/"
      ;;
    :)
      echo "$0: missing argument for option -${OPTARG}"
      echo "Try '$0 -h' for more information"
      exit 1
      ;;
    h)
      show_help
      exit 0
      ;;
    ?)
      echo "$0: invalid option -${OPTARG}"
      echo "Try '$0 -h' for more information"
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

if [ "$1" = "" ]
then
  echo "$0: missing operand"
  echo "Try '$0 -h' for more information"

  exit 1
fi

cat << EOF > "$outdir${EPOCHSECONDS}_${1}.py"
def up(connection):
    pass
EOF
